import React, { useEffect } from 'react';
import './App.css';
import axios from 'axios';

function App() {
  
  /*
  const llamadoFetch =  (url) => {
    fetch(url, 
      {mode: 'cors',headers: {'Access-Control-Allow-Origin': true, withCredentials: true, credentials: "same-origin"}})
      .then(response => console.log(response.json()))
      .then(data => console.log(data))

      
    
  } */
  const llamadoFetch = async (url) => {
    try {
      const res = await axios.get(url, {
        withCredentials: true,
        headers: {
          
        }
      });
      console.log(res)
      console.log(res.data)

      //const { root } = res.data;
      
    } catch (error) {
      console.error(error);
      return error;
    }
  };
//block-v1:S4NCampus+CB3+CB3_T1+type@html+block@3fb097f52c1a47d5ac3fd569d632cd60
//block-v1:S4NCampus+CB3+CB3_T1+type@html+block@a75e2ff179084de187b31032e391b6dc
//S4NCampus+CB3+CB3_T1
//S4NCampus%2BCB3%2BCB3_T1
//edX+DemoX+Demo_Course
//edX%2BDemoX%2BDemo_Course
  //'http://localhost:3000/api/courses/v1/blocks/?course_id=course-v1%3AedX%2BDemoX%2BDemo_Course'
  useEffect(()=>{
    llamadoFetch('/api/courses/v1/blocks/?course_id=course-v1%3AS4NCampus%2BCB3%2BCB3_T1&username=edx&depth=all')
  }, [])
  
  return (
    <div className="App">
      <header className="App-header">
      <iframe
        className="iframe-prueba"
        title='uno'
        src='/xblock/block-v1:S4NCampus+CB3+CB3_T1+type@html+block@3fb097f52c1a47d5ac3fd569d632cd60'
        allowFullScreen
        scrolling="no"
      />
      <iframe
        className="iframe-prueba"
        title='dos'
        src='/xblock/block-v1:S4NCampus+CB3+CB3_T1+type@html+block@a75e2ff179084de187b31032e391b6dc'
        allowFullScreen
        scrolling="no"
      />
      </header>
    </div>
  );
}

export default App;
