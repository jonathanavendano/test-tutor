
const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'http://lab.campus.s4n.co',
      changeOrigin: true,
      withCredentials: true,
      auth: 'edx@example.com:campus',
    })
  );
};
